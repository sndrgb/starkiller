import gsap from 'gsap';
import { qsa } from '../base/utils';

const transitions = {
    generic() {
        const tl = new gsap.TimelineMax({ onComplete: () => { this.done(); } });
        this.elsIn = qsa('[data-animation="in"]', this.newContainer);

        tl
        .add('start')
        .set(this.elsIn, {
            autoAlpha: 0,
            skewX: -20
        }, 'start')
        .fromTo(this.newContainer, 0.1, {
            autoAlpha: 0
        }, {
            autoAlpha: 1
        }, 'start')
        .staggerTo(this.elsIn, 1, {
            skewX: 0,
            autoAlpha: 1
        }, 0.05);
    },

    stagger() {
        const tl = new gsap.TimelineMax({ onComplete: () => { this.done(); } });
        this.setNewElement();

        tl.add('start')
        .fromTo(this.newContainer, 0.1, {
            autoAlpha: 0
        }, {
            autoAlpha: 1
        }, 'start')
        .staggerTo(this.elsIn, 2, {
            autoAlpha: 1
        }, 0.2);
    },

    fade() {
        gsap.fromTo(this.newContainer, 0.1, {
            autoAlpha: 0
        }, {
            autoAlpha: 1,
            onComplete: () => { this.done(); }
        }, 'start');
    }
};

export default transitions;