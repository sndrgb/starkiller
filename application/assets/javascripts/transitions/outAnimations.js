import gsap from 'gsap';
import { qsa } from '../base/utils';

const transitions = {
    generic() {
        return new Promise((resolve) => {
            const $elsOut = qsa('[data-animation="out"]', this.oldContainer);
            const tl = new gsap.TimelineMax({ onComplete: () => { resolve(); } });

            tl
            .staggerTo($elsOut, 1, {
                autoAlpha: 0
            }, 0.05)
            .set(this.oldContainer, {
                autoAlpha: 0
            });
        });
    },

    stagger() {
        return new Promise((resolve) => {
            const tl = new gsap.TimelineMax({ onComplete: () => { resolve(); } });
            this.setElements();

            tl.add('start')
            .staggerTo(this.elsOut, 1, {
                autoAlpha: 0
            }, 0.1)
            .set(this.oldContainer, {
                autoAlpha: 0
            });
        });
    }
};

export default transitions;