import gsap from 'gsap';
import preload from 'preload-js';

import Component from '../base/component';
import { addClass, toggleClass } from '../base/utils';

class Preloader extends Component {
    constructor(el, opts) {
        super(el, opts);

        this.completed = null;
        this.queue = new preload.LoadQueue(false);
    }

    init() {
        super.init();
        addClass(document.body, 'is-loading');
        this.queue.on('complete', this.allComplete, this);
        this.queue.loadManifest(this.options.assets, false);

        const tl = new gsap.TimelineMax({ onComplete: () => {
            this.queue.load();
        } });

        //here you can change the value of a to anything you need
        tl.add('start');

        this.queue.on('progress', (e) => {
            for (let i = 1; i < 2; i += 1) {
                const progress = parseInt(e.progress * 100, 10);

                const progressLine = Math.round((progress * 0.7) + 6);
                gsap.to('.progress', 0.6, { width: `${progressLine}%` });
            }
        });

        return new Promise((resolve) => { this.completed = resolve; });
    }

    allComplete() {
        setTimeout(() => {
            this.completed();
        }, 2000);
    }

    onComplete() {
        const percent = parseInt(this.queue.progress * 100);
        this.loaded.innerHTML = percent;
    }

    destroy() {
        super.destroy();

        addClass(document.body, 'is-loaded');
        toggleClass(document.body, 'is-loading');
    }
}

export default Preloader;