import Barba from 'barba.js';
import { addClass, toggleClass } from '../base/utils';

const Homepage = Barba.BaseView.extend({
    namespace: 'home',
    onEnter() {
        addClass(document.body, 'is-home');
    },

    onLeave() {
        toggleClass(document.body, 'is-home');
    }
});

Homepage.init();