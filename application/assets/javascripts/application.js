/**
 * Main Application File
 *
 * Use for bootstrapping large application
 * or just fill it with JS on small projects
 *
 */

/**
 * This is a test script
 */

import classie from 'classie';
import Barba from 'barba.js';
import * as manifest from './manifest.json';
import { data, qsa } from './base/utils';
import Loader from './components/loader';
import {
    GenericTransition,
    HomeToAll
} from './transitions';
import './sections/home';


const links = qsa('a[href]', document);
links.forEach((el) => {
    el.addEventListener('click', (e) => {
        if (e.currentTarget.href === window.location.href) {
            e.preventDefault();
            e.stopPropagation();
        }
    });
});

// const noise = new Noise();
// noise.loop();

document.addEventListener('DOMContentLoaded', () => {
    const menuVoices = qsa('[data-nav]');
    const getMenuVoice = (els, namespace) => els.find(el => el.getAttribute('data-namespace', namespace) === namespace);
    let lastElementClicked;
    let currentNamespace;

    // start up
    const loader = new Loader('#loader', { assets: manifest });
    loader.init().then(() => {
        Barba.Pjax.start();
        Barba.Prefetch.init();
        loader.destroy();
    });

    // get current namespace
    Barba.Dispatcher.on('linkClicked', (el) => {
        lastElementClicked = el;
        currentNamespace = data('namespace', lastElementClicked);
    });

    // set class to current menu voice
    Barba.Dispatcher.on('newPageReady', (view) => {
        const currentMenuVoice = getMenuVoice(menuVoices, view.namespace);
        if (currentMenuVoice !== undefined && currentMenuVoice !== null) {
            classie.add(currentMenuVoice, 'is-active');
        }
    });

    // get right transition
    Barba.Pjax.getTransition = () => {
        let transition;
        const prevNamespace = Barba.Pjax.History.prevStatus().namespace;
        const prevMenu = getMenuVoice(menuVoices, prevNamespace);

        if (prevMenu !== undefined && prevMenu !== null) {
            classie.remove(prevMenu, 'is-active');
        }

        if (homeToAll.isValid(prevNamespace, currentNamespace)) {
            transition = HomeToAll;
        } else {
            transition = GenericTransition;
        }

        return transition;
    };
});